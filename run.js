exports.main = function(){
	const http = require('http');
	const fs = require('fs-extra');
	const unzip = require('unzip');
	const git = require('simple-git');

	//const start= "February 3, 2017";
	const start= "September 1, 2017";

	var date = new Date(Date.parse(start));
	var today = new Date();

	const datadir = 'data/';
	const link = 'http://ns.translink.ca/gtfs/google_transit.zip'

	fs.ensureDirSync(datadir);
	console.log("Downloading most recent data");
	var file = fs.createWriteStream(datadir + "/google_transit.zip");
	var request = http.get(link, (response) => {
		response.pipe(file);
		file.on('finish',() => {
			console.log("Downloaded data, extracting...");
			fs.createReadStream(datadir + "/google_transit.zip").pipe(unzip.Extract({ path: datadir + "/" })
				.on('finish',() =>{
					console.log("Data extracted, cleaning up...");	
					fs.unlink(datadir + "/google_transit.zip");
					console.log("Commiting changes...");
					function commitAll(){
						git().raw(
							[
								'add',
								'-A',
							], (err, result) => {
								if(err){
									console.log(err);
									return;
								}
								git().commit("Updated data");
							}
						);
					}
					//Wait a bit for files to write
					setTimeout(commitAll,2000);
				}).on("error",(err) => {
					//console.log(err);
					console.log("Error extracting data");
					return;
				}));
		});
	}).on("error", (err) => {
		console.log(err);
		console.log("Error downloading data");
		fs.unlink(datadir + "/google_transit.zip");
		return;
	});
}

